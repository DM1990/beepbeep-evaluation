package com.example.beepbeepevaluation;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

import com.example.beepbeepevaluation.microphone.MicrophoneControlling;
import com.example.beepbeepevaluation.network.NetworkControlling;
import com.example.beepbeepevaluation.speaker.AudioOutputThread;
import com.example.beepbeepevaluation.speaker.SpeakerControlling;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /* Start required subsystems */
        Globals.SubSystems.SPEAKER_CONTROLLING = SpeakerControlling.getInstance();
        Globals.SubSystems.MICROPHONE_CONTROLLING = MicrophoneControlling.getInstance();
        Globals.SubSystems.NETWORK_CONTROLLING = NetworkControlling.getInstance();
        
        Globals.ACTIVITY_REF = this;
        
        setContentView(R.layout.activity_start);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /* Button handler */
    public void hndlGetMyAddress(View view) {
    	try {
			String address = InetAddress.getLocalHost().getHostAddress().toString();
		} catch (UnknownHostException e) {
			Log.e(Globals.APP_NAME, "Could not return local-address! (" + e.getMessage() + ")");
			return;
		}
    	return;
    }
    
    public void hndlSetRemoteAddress(View view) {
    	ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set remote address (IP:Port)");
    	cfg.setDialogTitel("Remote Address");
    	cfg.setConfigKey("Address.Remote");
    	cfg.show();
    }
    
    public void hndlSetKValue(View view) {
    	ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the K value of current device (double)");
    	cfg.setDialogTitel("K value");
    	cfg.setConfigKey("SignalProcessing.K");
    	cfg.show();
    }
    
    public void hndlSetThrSig01Value(View view) {
    	ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the threshold for SIG01 (double)");
    	cfg.setDialogTitel("Threshold SIG01");
    	cfg.setConfigKey("SignalProcessing.THSIG01");
    	cfg.show();
    }

	public void hndlSetThrSig02Value(View view) {
		ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the threshold for SIG02 (double)");
    	cfg.setDialogTitel("Threshold SIG02");
    	cfg.setConfigKey("SignalProcessing.THSIG02");
    	cfg.show();
	}
	
	public void hndlPlay440Hz1s(View view) {
		Globals.SubSystems.SPEAKER_CONTROLLING.playTone( 
												(float) 440.0, 
												(float) 1.0 );
	}
	
	public void hndlSetTempFolderPath(View view) {
		ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the Temp-Folder path");
    	cfg.setDialogTitel("Temp-Folder");
    	cfg.setConfigKey("System.TempFolder");
    	cfg.show();
	}
	
	public void hndlRec5sMic(View view) {
		(new RecFiveSecondsWithMicrophone()).start();
	}
	
	public void hndlConnect(View view) {
		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				Globals.SubSystems.NETWORK_CONTROLLING.start();
				Globals.SubSystems.NETWORK_CONTROLLING.connect();
				
			}});
		th.start();
	}
}

/* Utility classes */

class RecFiveSecondsWithMicrophone extends Thread {
	
	public RecFiveSecondsWithMicrophone() {
		this.setName("FiveSecRecordingThread");
	}
	
	public void run() {
		
		
		/* Wait until microphone is ready */
		Globals.SubSystems.MICROPHONE_CONTROLLING.waitForStop();
		
		/* Start recording */
		Globals.SubSystems.MICROPHONE_CONTROLLING.startRecord((float) 5.0);
		
		/* Wait until recording finished */
		Globals.SubSystems.MICROPHONE_CONTROLLING.waitForStop();
		
		/* Get configured path */
		String expPath = Globals.ACTIVITY_REF.getSharedPreferences( Globals.CONFIG_NAME, 0 ).getString("System.TempFolder", null);
		if(expPath == null) {
			Log.e(Globals.APP_NAME, "No path for temporary folder had been specified! => Export impossible!");
			return;
		} else {
			expPath = expPath + Long.toHexString((new Random()).nextLong()) + ".pcm";
		}
		
		/* Store the content to a predifined path */
		Globals.SubSystems.MICROPHONE_CONTROLLING.storeRecordedPcm(expPath);
		
		/* Notify the user about successful completion */
		final String finPath = expPath;
		Globals.ACTIVITY_REF.runOnUiThread( new Runnable() {

			@Override
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(Globals.ACTIVITY_REF);
				builder.setMessage("Microphone-Output had been stored! \n PATH : " + finPath);
				builder.setTitle("Process completed!");
				AlertDialog alert = builder.create();
				alert.show();
				
			}
			
		});
	}
}
