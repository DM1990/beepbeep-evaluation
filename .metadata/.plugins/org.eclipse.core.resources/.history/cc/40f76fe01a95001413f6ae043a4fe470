package com.example.beepbeepevaluation.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Map;

import com.example.beepbeepevaluation.Globals;

import android.util.Log;

public class OpcodeServerThread extends Thread {

	private Socket mSocket = null;
	
	private boolean isAlive = true;
	
	private InputStreamReader mIn = null;
	private OutputStreamWriter mOut = null;
	
	public OpcodeServerThread(Socket soc) {
		this.mSocket = soc;
	}
	
	public boolean isConnectedToRemoteSide() {
		return this.mSocket.isConnected();
	}
	
	public boolean isStillAlive() {
		return this.isAlive;
	}
	
	public void killSever() {
		this.isAlive = false;
	}
	
	public void run() {
		
		/* Initialize required references */
		try {
			this.mIn = new InputStreamReader(this.mSocket.getInputStream(), "UTF-8");
			this.mOut = new OutputStreamWriter(this.mSocket.getOutputStream(), "UTF-8");
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "OpcodeServerThread could not open IO Streams! (" + e.getMessage() + ")");
			this.isAlive = false;
		}
		
		/* Wait for messages as long, as there are no connection problem */
		while(this.isAlive) {
			
			/* Read the length-tag */
			int requestLength = 0;
			try {
				StringBuffer strBuf01 = new StringBuffer();
				while(true) {
					int nextByte = this.mIn.read();
					
					if(nextByte == -1) {
						throw new Exception("Closed connection while reading length-tag!");
					}
					
					char nextChar = (char) ( nextByte );
					if(nextChar != ' ') {
						strBuf01.append(nextChar);
					} else {
						requestLength = Integer.parseInt( strBuf01.toString() );
						break;
					}
				}
			} catch (Exception e) {
				Log.e(Globals.APP_NAME, "Could not receive the lenth-tag from client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
			
			/* Read the json-string */
			StringBuffer strBuf02 = new StringBuffer();
			try {
				while(requestLength > 0) {
					char[] tmpInBuffer = new char[requestLength]; 
					int receivedBytes = this.mIn.read(tmpInBuffer);
					strBuf02.append(tmpInBuffer, 0, receivedBytes);
					requestLength -= receivedBytes;
				}
			} catch (IOException e) {
				Log.e(Globals.APP_NAME, "Could not receive the request from client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
			
			/* Convert to message */
			Message requestMsg = Message.parseJsonObjToMessage( strBuf02.toString() );
			
			/* Call handler function */
			Message responseMsg = handleRequest(requestMsg);
			
			/* Convert result to json-string */
			String responseJsonString = responseMsg.toJsonString();
			
			/* Transmit the length-tag of the result */
			String msgLengthTag = String.valueOf( responseJsonString.length() ) + " ";
			try {
				this.mOut.write(msgLengthTag);
				this.mOut.flush();
			} catch (IOException e) {
				Log.e(Globals.APP_NAME, "Could not send the resonse length-tag to client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
			
			/* Transmit the result */
			try {
				this.mOut.write(responseJsonString);
				this.mOut.flush();
			} catch (IOException e) {
				Log.e(Globals.APP_NAME, "Could not send the response to the client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
		}
		
		/* Close socket */
		try {
			this.mSocket.close();
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not close the socket of the OpcodeServerThread! (" + e.getMessage() + ")");
		}
		
		/* Thread says goodbye */
		this.isAlive = false;
	}
	
	private Message handleRequest(Message in) {
		
		Message out = new Message();
		
		/* Read the opcode of request */
		String reqOpcode = in.get("Opcode");
		if(reqOpcode == null) {
			out.put("error", "The request did not contain an opcode!");
			return out;
		}
		
		/* Handle the opcode */
		if(reqOpcode.equals("GetState")) {
			hdnlOpcodeGetState(in, out);
		} else if(reqOpcode.equals("GetState")) {
		} else {
			out.put("error", "The passed opcode could not be resolved!");
		}
		
		/* Return the result */
		out.put("Opcode", reqOpcode);
		out.put("Response", "true");
		return out;
	}
	
	/* Opcode Handlers */
	private void hdnlOpcodeGetState(Message in, Message out) {
		/* Get state of audio recorder */
		boolean recorderAlive = Globals.SubSystems.MICROPHONE_CONTROLLING.isAlive();
		
		/* Get state of audio speaker */
		boolean speakerAlive = Globals.SubSystems.SPEAKER_CONTROLLING.isAlive();
		
		/* Append both results to the message */
		out.put("MicrophoneState", String.valueOf(recorderAlive));
		out.put("SpeakerState", String.valueOf(speakerAlive));
	}
}
