package com.example.beepbeepevaluation.network;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import com.example.beepbeepevaluation.Globals;

import android.util.JsonReader;
import android.util.JsonWriter;
import android.util.Log;

public class NetworkControlling {
	private static NetworkControlling SINGLETON = null;
	
	/* Network configurations */
	private InetAddress mAddress = null;
	private int mPort = 30000;
	private String mLocalIpAddress = "";
	
	/* SeverSocket reference */
	private ServerSocket mServerSocket = null;
	
	/* Socket references */
	private Socket mLocalSocket = null;
	private Socket mRemoteSocket = null;
	
	/* Json Reader & Writer for communication with the RemoteSocket*/
	private InputStreamReader mRsInpStreamReader = null;
	private OutputStreamWriter mRsOutStreamWriter = null;
	
	private boolean mNetworkInitialized = false;
	private OpcodeServerThread mOpcodeThread = null;
	
	private NetworkControlling() {
		return;
	}
	
	public static NetworkControlling getInstance() {
		if(SINGLETON == null) {
			SINGLETON = new NetworkControlling();
			
			/* What a brainfuck, but its Android */
			Runnable run = new Runnable() {

				@Override
				public void run() {
					try {
						Enumeration e = NetworkInterface.getNetworkInterfaces();
						NetworkControlling.SINGLETON.mLocalIpAddress = InetAddress.getLocalHost().getHostAddress().toString();
					} catch (Exception e) {
						Log.e(Globals.APP_NAME, "Could not find out the local IP address! (" + e.getMessage() + ")");
						return;
					}
				}
				
			};
			
			Thread th = new Thread(run);
			th.start();
			try {
				th.join();
			} catch (InterruptedException e) {
				Log.e(Globals.APP_NAME, "Could not join the ip-lookup-thread! (" + e.getMessage() + ")");
				return null;
			}
		}
		
		return SINGLETON;
	}
	
	public void setRemoteAddress(String address) {
		if(address != null && address.split(":").length != 2) {
			this.mAddress = null;
			Log.e(Globals.APP_NAME, "The passed IP-Address (" + address + ")is not valid!");
		} else {
			String tmpAdr = address.split(":")[0];
			String tmpPort = address.split(":")[1];
			try {
				this.mAddress = InetAddress.getByName(tmpAdr);
				this.mPort = Integer.parseInt(tmpPort);
			} catch (UnknownHostException e) {
				Log.e(Globals.APP_NAME, "Could not resolve the passed address (" + address + ") !");
			}
		}
	}
	
	public void start() {
		/* Check if there is a valid address assigned */
		if(this.mAddress == null) {
			Log.e(Globals.APP_NAME, "Start of NetworkControlling is not possible without a specified address!");
			return;
		}
		
		/* Open a local TCP port */
		try {
			this.mServerSocket = new ServerSocket(this.mPort);
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Creation of ServerSocket was not possible (" + this.mPort + ") !");
			return;
		}
		
		/* See the networking as initialized */
		this.mNetworkInitialized = true;
	}
	
	public void connect() {
		
		/* Check if already started */
		if(!this.mNetworkInitialized) {
			Log.e(Globals.APP_NAME, "Tried to connect before staring NetworkControlling!");
			return;
		}
		
		/* Close already opened connections if exists */
		/* ... kill OpcodeServerThread if running */
		if(this.mOpcodeThread != null) {
			if(this.mOpcodeThread.isAlive()) {
				this.mOpcodeThread.killSever();
				try 
				{
					this.mOpcodeThread.join();
				} catch (InterruptedException e) {
					Log.e(Globals.APP_NAME, "Could not joint to allready existing OpcodeServerThread! (" + e.getMessage() + ")");
					return;
				}
			} else {
				this.mOpcodeThread = null;
			}
		}
		/* ... close connection with remote ServerSocket if opened */
		if(this.mRemoteSocket != null) {
			if(!this.mRemoteSocket.isClosed()) {
				try {
					this.mRemoteSocket.close();
				} catch (IOException e) {
					Log.e(Globals.APP_NAME, "Could not close connection to remote ServerSocket! (" + e.getMessage() + ")");
					return;
				}
			}
		}
		
		/* ... set both socket references to null */
		this.mLocalSocket = null;
		this.mRemoteSocket = null;
		
		/* Iterate until connection to local and remote port had been established */
		while(true) {
			
			/* ... try to catch someone at my ServerSocket */
			if(this.mLocalSocket == null) {
				try {
					this.mServerSocket.setSoTimeout(1000);
					this.mLocalSocket = this.mServerSocket.accept();
					this.mOpcodeThread = new OpcodeServerThread(this.mLocalSocket);
				} catch (SocketException e) {
					Log.e(Globals.APP_NAME, "Could not configure timeout for local ServerSocket! (" + e.getMessage() + ")");
				} catch (IOException e) {
					Log.w(Globals.APP_NAME, "Timeout exeeded before requests to local port had reached! (" + e.getMessage() + ")");
				}
				
			}
			
			/* ... try to connect with a remote ServerSocket */
			else if(this.mRemoteSocket == null) {
				try {
					this.mRemoteSocket = new Socket(this.mAddress, this.mPort);
					this.mRsInpStreamReader = new InputStreamReader(this.mRemoteSocket.getInputStream(), "UTF-8");
					this.mRsOutStreamWriter = new OutputStreamWriter(this.mRemoteSocket.getOutputStream(), "UTF-8"); 
				} catch (IOException e) {
					Log.w(Globals.APP_NAME, "Connection with the remote ServerSocket had been failed! (" + e.getMessage() + ")");
				}
			}
			
			/* ... break in case both connections could be established */
			else {
				break;
			}
			
			/* Be a gentleman */
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Log.e(Globals.APP_NAME, "Sleeping is too overestimated these days! (" + e.getMessage() + ")");
				return;
			}
		}
	}
	
	public String getLocalAddress() {
		return this.mLocalIpAddress;
	}
	
	public Message sendRequest(Message msg) {
		
		/* Convert message to json-String */
		String msgJson = msg.toJsonString();
		
		/* Send the length-tag */
		String msgLengthTag = String.valueOf( msgJson.length() ) + " ";
		try {
			this.mRsOutStreamWriter.write(msgLengthTag);
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not send the length-tag! (" + e.getMessage() + ")");
			return null;
		}
		
		/* Send the message */
		try {
			this.mRsOutStreamWriter.write(msgJson);
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not send the messge to remote socket! (" + e.getMessage() + ")");
			return null;
		}
		
		/* Wait for length-tag */
		int answerLength = 0;
		try {
			StringBuffer strBuf01 = new StringBuffer();
			while(true) {
				char nextChar = (char) ( this.mRsInpStreamReader.read() );
				if(nextChar != ' ') {
					strBuf01.append(nextChar);
				} else {
					answerLength = Integer.parseInt( strBuf01.toString() );
					break;
				}
			}
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not receive the lenth-tag from remote socket! (" + e.getMessage() + ")");
			return null;
		}
		
		/* Receive json-String*/
		StringBuffer strBuf02 = new StringBuffer();
		try {
			while(answerLength > 0) {
				char[] tmpInBuffer = new char[answerLength]; 
				int receivedBytes = this.mRsInpStreamReader.read(tmpInBuffer);
				strBuf02.append(tmpInBuffer, 0, receivedBytes);
				answerLength -= receivedBytes;
			}
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not receive the answer from remote socket! (" + e.getMessage() + ")");
			return null;
		}
		
		/* Convert to message */
		return Message.parseJsonObjToMessage( strBuf02.toString() );
	}
}
