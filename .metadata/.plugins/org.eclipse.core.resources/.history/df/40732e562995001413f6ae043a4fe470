package com.example.beepbeepevaluation;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

import com.example.beepbeepevaluation.microphone.MicrophoneControlling;
import com.example.beepbeepevaluation.network.Message;
import com.example.beepbeepevaluation.network.NetworkControlling;
import com.example.beepbeepevaluation.speaker.AudioOutputThread;
import com.example.beepbeepevaluation.speaker.SpeakerControlling;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class StartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        /* Start required subsystems */
        Globals.SubSystems.SPEAKER_CONTROLLING = SpeakerControlling.getInstance();
        Globals.SubSystems.MICROPHONE_CONTROLLING = MicrophoneControlling.getInstance();
        Globals.SubSystems.NETWORK_CONTROLLING = NetworkControlling.getInstance();
        
        Globals.ACTIVITY_REF = this;
        
        setContentView(R.layout.activity_start);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /* Button handler */
    public void hndlGetMyAddress(View view) {
		Globals.showMessageBox("My current IP-Address", Globals.SubSystems.NETWORK_CONTROLLING.getLocalAddress() );
    }
    
    public void hndlSetRemoteAddress(View view) {
    	ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set remote address (IP:Port)");
    	cfg.setDialogTitel("Remote Address");
    	cfg.setConfigKey("Address.Remote");
    	cfg.show();
    }
    
    public void hndlSetKValue(View view) {
    	ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the K value of current device (double)");
    	cfg.setDialogTitel("K value");
    	cfg.setConfigKey("SignalProcessing.K");
    	cfg.show();
    }
    
    public void hndlSetThrSig01Value(View view) {
    	ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the threshold for SIG01 (double)");
    	cfg.setDialogTitel("Threshold SIG01");
    	cfg.setConfigKey("SignalProcessing.THSIG01");
    	cfg.show();
    }

	public void hndlSetThrSig02Value(View view) {
		ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the threshold for SIG02 (double)");
    	cfg.setDialogTitel("Threshold SIG02");
    	cfg.setConfigKey("SignalProcessing.THSIG02");
    	cfg.show();
	}
	
	public void hndlPlay440Hz1s(View view) {
		Globals.SubSystems.SPEAKER_CONTROLLING.playTone( 
												(float) 440.0, 
												(float) 1.0 );
	}
	
	public void hndlSetTempFolderPath(View view) {
		ConfigurationInputDialog cfg = new ConfigurationInputDialog(this);
    	cfg.setDialogMessage("Set the Temp-Folder path");
    	cfg.setDialogTitel("Temp-Folder");
    	cfg.setConfigKey("System.TempFolder");
    	cfg.show();
	}
	
	public void hndlRec5sMic(View view) {
		(new RecFiveSecondsWithMicrophone()).start();
	}
	
	public void hndlConnect(View view) {
		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				Globals.SubSystems.NETWORK_CONTROLLING.setRemoteAddress( Globals.ACTIVITY_REF.getSharedPreferences( Globals.CONFIG_NAME, 0 ).getString("Address.Remote", null) );
				Globals.SubSystems.NETWORK_CONTROLLING.start();
				Globals.SubSystems.NETWORK_CONTROLLING.connect();
				
			}});
		th.start();
	}
	
	public void hndlTestMeasurement(View view) {
		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				/* Start recording*/
				Globals.SubSystems.MICROPHONE_CONTROLLING.startRecord(Globals.RECORD_DURATION);
				
				/* Sleep for a while */
				try {
					Thread.sleep(Globals.BREAK_TIME_IN_MS);
				} catch (InterruptedException e) {
					return;
				}
				
				/* Play signal 01 */
				/* Sleep for a while */
				/* Play signal 02 */
				/* Wait until record is finished */
				/* Perform storage */
				
			}});
		th.start();
	}
}

/* Utility classes */

class RecFiveSecondsWithMicrophone extends Thread {
	
	public RecFiveSecondsWithMicrophone() {
		this.setName("FiveSecRecordingThread");
	}
	
	public void run() {
		
		
		/* Wait until microphone is ready */
		Globals.SubSystems.MICROPHONE_CONTROLLING.waitForStop();
		
		/* Start recording */
		Globals.SubSystems.MICROPHONE_CONTROLLING.startRecord((float) 5.0);
		
		/* Wait until recording finished */
		Globals.SubSystems.MICROPHONE_CONTROLLING.waitForStop();
		
		/* Get configured path */
		String expPath = Globals.ACTIVITY_REF.getSharedPreferences( Globals.CONFIG_NAME, 0 ).getString("System.TempFolder", null);
		if(expPath == null) {
			Log.e(Globals.APP_NAME, "No path for temporary folder had been specified! => Export impossible!");
			return;
		} else {
			expPath = expPath + Long.toHexString((new Random()).nextLong()) + ".pcm";
		}
		
		/* Store the content to a predifined path */
		Globals.SubSystems.MICROPHONE_CONTROLLING.storeRecordedPcm(expPath);
		
		/* Notify the user about successful completion */
		final String finPath = expPath;
		Globals.showMessageBox(
				"Process completed!",
				"Microphone-Output had been stored! \n PATH : " + finPath);
	}
}

class PerformRangeMeasurement extends Thread {
	
	public void run() {
		/* STEP 01 - Start my microphone */
		Globals.SubSystems.MICROPHONE_CONTROLLING.startRecord(Globals.RECORD_DURATION);
		
		/* STEP 02 - Request starting microphone of remote device */
		Message msgStep02 = new Message();
		msgStep02.put("Opcode", "StartRecording");
		msgStep02.put("Duration", String.valueOf(Globals.RECORD_DURATION) );
		Message ansStep02 = Globals.SubSystems.NETWORK_CONTROLLING.sendRequest(msgStep02);
		
		/* STEP 03 - Wait 0.25s */
		try {
			Thread.sleep(Globals.BREAK_TIME_IN_MS);
		} catch (InterruptedException e) {
			Log.e(Globals.APP_NAME, "Measurement failed on Step 03! (" + e.getMessage() + ")");
			return;
		}
		
		/* STEP 04 - Start playing a tone from my device */
		Globals.SubSystems.SPEAKER_CONTROLLING.playTone(Globals.SIGNAL_FREQUENCE, Globals.SIGNAL_DURATION);
		
		/* STEP 05 - Wait until I am finished with playing */
		Globals.SubSystems.SPEAKER_CONTROLLING.waitForStopState();
		
		/* STEP 06 - Wait 0.25s */
		try {
			Thread.sleep(Globals.BREAK_TIME_IN_MS);
		} catch (InterruptedException e) {
			Log.e(Globals.APP_NAME, "Measurement failed on Step 06! (" + e.getMessage() + ")");
			return;
		}
		
		/* STEP 07 - Request playing the same tone from remote device */
		Message msgStep07 = new Message();
		msgStep07.put("Opcode", "PlayTone");
		msgStep07.put("Frequence", String.valueOf(Globals.SIGNAL_FREQUENCE) );
		msgStep07.put("Duration", String.valueOf(Globals.SIGNAL_DURATION) );
		Message ansStep07 = Globals.SubSystems.NETWORK_CONTROLLING.sendRequest(msgStep07);
		
		/* STEP 08 - Wait until that device is finished with playing */
		Message msgStep08 = new Message();
		msgStep08.put("Opcode", "WaitForSilence");
		Message ansStep08 = Globals.SubSystems.NETWORK_CONTROLLING.sendRequest(msgStep08);
		
		/* STEP 09 - Wait until my record has been stopped */
		Globals.SubSystems.MICROPHONE_CONTROLLING.waitForStop();
		
		/* STEP 10 - Wait for remote record stopping */
		Message msgStep10 = new Message();
		msgStep10.put("Opcode", "WaitForRecordStopped");
		Message ansStep10 = Globals.SubSystems.NETWORK_CONTROLLING.sendRequest(msgStep10);
		
		/* STEP 11 - Request analysis of recorded signal from remote device */
		/* TODO */
		
		/* STEP 12 - Perform analysis of self recorded signal */
		/* TODO */
		
		/* STEP 13 - Perform evaluation of both analysis */
		/* TODO */
		
		/* STEP 14 - Display the calculated range on the screen */
		/* TODO */
	}
	
}
