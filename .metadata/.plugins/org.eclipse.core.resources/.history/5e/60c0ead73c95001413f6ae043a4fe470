package com.example.beepbeepevaluation.speaker;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

public class AudioOutputThread extends Thread {
	
	/* Class configuration */
	private static final int MAX_SAMPLE_PER_ITER = 10000; 
	private static final int SAMPLING_RATE = 44100;
	
	/* PCM Buffer */
	private short[] mPcmBuffer = new short[0];
	private int		mPcmBufferCurPos = 0;
	
	/* FSM State */
	private Request mLastReq = Request.INITIALIZE;
	
	/* Audio subsystem references */
	private AudioTrack mAudioTrack = null;
	
	/* Events */
	private Semaphore mWakeUpSem 		= new Semaphore(0);
	private Semaphore mInitDone 		= new Semaphore(0);
	private Semaphore mStopStateReached = new Semaphore(0);
	
	/* Error Message */
	private Exception mLastError = null;
	
	public AudioOutputThread() {
		this.setName("AudioOutputThread");
	}
	
	public void run() {
		while(true) {
			switch(mLastReq) {
				case INITIALIZE:
					hndlReqInitialize();
					break;
				case STOP:
					hndlReqStop();
					break;
				case PLAY:
					hndlReqPlay();
					break;
				case PAUSE:
					hndlReqPause();
					break;
				case DESTROY:
					hndlReqDestroy();
					return;	
				case ERROR:
					hndlReqError();
					break;
			}
		}
	}
	
	/* Request handler */
	private void hndlReqInitialize() {
		
		int intSize = AudioTrack.getMinBufferSize(
										SAMPLING_RATE, 
										AudioFormat.CHANNEL_OUT_MONO,
										AudioFormat.ENCODING_PCM_16BIT ); 

		this.mAudioTrack = new AudioTrack(
								AudioManager.STREAM_MUSIC, 
								SAMPLING_RATE, 
								AudioFormat.CHANNEL_OUT_MONO,
								AudioFormat.ENCODING_PCM_16BIT, 
								intSize, 
								AudioTrack.MODE_STREAM); 
		
		if(this.mAudioTrack == null) {
			this.mLastError = new Exception("Could not initialize the AudioTrack!");
			mLastReq = Request.ERROR;
		} else {
			this.mAudioTrack.play();
		}
		
		if(this.mPcmBuffer.length > 0) {
			mLastReq = Request.PLAY;
		} else {
			mLastReq = Request.STOP;
		}
		
		mInitDone.release();
	}
	
	private void hndlReqStop() {
		try {
			if(this.mStopStateReached.availablePermits() == 0)
				this.mStopStateReached.release();
			
			mWakeUpSem.acquire();
			
			if(this.mStopStateReached.availablePermits() > 0)
				this.mStopStateReached.acquire();
			
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}
		return;
	}
	
	private void hndlReqPause() {
		try {
			mWakeUpSem.acquire();
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}
		return;
	}
	
	private void hndlReqPlay() {
		int samplesToWrite = Math.min(this.mPcmBuffer.length - 1 - this.mPcmBufferCurPos, MAX_SAMPLE_PER_ITER);
		
		if(samplesToWrite > 0) {
			this.mPcmBufferCurPos += mAudioTrack.write(mPcmBuffer, mPcmBufferCurPos, samplesToWrite);
		} else {
			mLastReq = Request.STOP;
		}
		return;
	}
	
	private void hndlReqDestroy() {
		mAudioTrack.stop(); 
		mAudioTrack.release();
		return;
	}
	
	private void hndlReqError() {
		try {
			mWakeUpSem.acquire();
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}
	}
	
	/* Public methods */
	public void playPcm(short[] pcm) {
		if( (pcm != null) && (this.mLastReq != Request.PLAY) ) {
			this.mPcmBuffer = pcm;
			this.mPcmBufferCurPos = 0;
			this.mLastReq = Request.PLAY;
			this.mWakeUpSem.release();
		}
	}
	
	public void stopPlaying() {
		this.mLastReq = Request.STOP;
	}
	
	public void waitForInit() {
		if(this.mLastReq != Request.INITIALIZE) {
			return;
		} else {
			try {
				mInitDone.acquire();
			} catch (InterruptedException e) {
				mLastError = e;
				mLastReq = Request.ERROR;
			}
		}
	}
	
	public void waitForStopState() {
		if(this.mLastReq == Request.STOP)
			return;
		else {
			try 
			{
				this.mStopStateReached.acquire();
			} catch (InterruptedException e) {
				mLastError = e;
				mLastReq = Request.ERROR;
			}
		}
	}
	
	public void playTone(float frequence, float duration) {
		if( (this.mLastReq != Request.PLAY)
				&& (frequence > 0)
				&& (duration > 0) ) 
		{
			int sampleNumber = (int) (((float) SAMPLING_RATE) * duration);
			float SecPerSample = (float) (1.0 / ((float)SAMPLING_RATE));
			this.mPcmBuffer = new short[sampleNumber];
			this.mPcmBufferCurPos = 0;
			
			for(int i = 0; i < mPcmBuffer.length; i++) {
				//this.mPcmBuffer[i] = (short) (((float)Short.MAX_VALUE) * Math.cos( SecPerSample * ((float)i) * frequence * 2.0 *  Math.PI));
				float freqPercent = ((float) i) / ((float) mPcmBuffer.length );
				this.mPcmBuffer[i] = (short) (((float) 2000) * Math.sin( SecPerSample * ((float)i) * frequence * 2.0 *  Math.PI));
			}
			
			/* Perform curshion */
			//float newFreq = 1/(duration*2);
			//for(int i = 0; i < mPcmBuffer.length; i++) {
			//	this.mPcmBuffer[i] = (short) (((float)this.mPcmBuffer[i]) * Math.pow(Math.sin( SecPerSample * ((float)i) * newFreq * 2.0 *  Math.PI),20.0));
			//}
			
			this.mLastReq = Request.PLAY;
			this.mWakeUpSem.release();
		}
	}
	
	public boolean isStillAlive() {
		if(this.mLastReq != Request.INITIALIZE && this.mLastReq != Request.ERROR) {
			return true;
		} else {
			return false;
		}
	}
	
	enum Request {
		INITIALIZE,
		STOP,
		PAUSE,
		PLAY,
		DESTROY,
		ERROR
	}
}
