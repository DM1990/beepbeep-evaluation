import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map.Entry;


public class Program {
	
	public static final int SOCKET_PORT = 30000;
	public static final String SOCKET_ADDRESS = "192.168.173.216";
	
	private static ServerSocket mMyServerSocket = null;
	
	private static Socket mLocalServerConnection = null;
	private static Socket mRemoteServerConnection = null;
	
	private static InputStreamReader 	sInpStream = null;
	private static OutputStreamWriter 	sOutStream = null; 
	
	private static InputStreamReader 	sLocInpStream = null;
	private static OutputStreamWriter 	sLocOutStream = null; 
	
	public static void main(String[] args) throws Exception {
		/* Start my own server socket */
		System.out.println(">> START SERVER SOCKET !");
		mMyServerSocket = new ServerSocket(SOCKET_PORT);
		
		/* Try to connect to a remote server-socket */
		System.out.println(">> TRY TO CONNECT TO REMOTE SERVER SOCKET !");
		InetAddress adr = InetAddress.getByName(SOCKET_ADDRESS);
		mRemoteServerConnection = new Socket(adr, SOCKET_PORT);
		System.out.println(">> CONNECTION TO REMOTE SERVER SOCKET ESTABLISHED !");
		
		/* Wait for a client */
		System.out.println(">> WAIT FOR A CLIENT !");
		mLocalServerConnection = mMyServerSocket.accept();
		System.out.println(">> CLIENT CONNECTED !");
		
		/* Create streams for communication to remote server-socket */
		sInpStream = new InputStreamReader(mRemoteServerConnection.getInputStream(), "UTF-8");
		sOutStream = new OutputStreamWriter(mRemoteServerConnection.getOutputStream(), "UTF-8"); 
		
		/* Create streams for communication to local server-socket*/
		sLocInpStream = new InputStreamReader(mLocalServerConnection.getInputStream(), "UTF-8");
		sLocOutStream = new OutputStreamWriter(mLocalServerConnection.getOutputStream(), "UTF-8"); 
		
		/* Start sending requests */
		//sendingRequests();
		
		/* Start process requests */
		processRequests();
		
		/* Close sockets */
		mLocalServerConnection.close();
		mRemoteServerConnection.close();
		mMyServerSocket.close();
		
		/* Goodbye */
		System.out.println(">> DEMO FINISHED !");
	}
	
	private static Message performRequest(Message msg) {
		/* Convert message to json-String */
		String msgJson = msg.toJsonString();
		
		/* Send the length-tag */
		String msgLengthTag = String.valueOf( msgJson.length() ) + " ";
		try {
			sOutStream.write(msgLengthTag);
			sOutStream.flush();
		} catch (IOException e) {
			return null;
		}
		
		/* Send the message */
		try {
			sOutStream.write(msgJson);
			sOutStream.flush();
		} catch (IOException e) {
			return null;
		}
		
		/* Wait for length-tag */
		int answerLength = 0;
		try {
			StringBuffer strBuf01 = new StringBuffer();
			while(true) {
				char nextChar = (char) ( sInpStream.read() );
				if(nextChar != ' ') {
					strBuf01.append(nextChar);
				} else {
					answerLength = Integer.parseInt( strBuf01.toString() );
					break;
				}
			}
		} catch (IOException e) {
			return null;
		}
		
		/* Receive json-String*/
		StringBuffer strBuf02 = new StringBuffer();
		try {
			while(answerLength > 0) {
				char[] tmpInBuffer = new char[answerLength]; 
				int receivedBytes = sInpStream.read(tmpInBuffer);
				strBuf02.append(tmpInBuffer, 0, receivedBytes);
				answerLength -= receivedBytes;
			}
		} catch (IOException e) {
			return null;
		}
		
		/* Convert to message */
		return Message.parseJsonObjToMessage( strBuf02.toString() );
	}
	
	private static void sendingRequests() throws Exception {
	
		/* Get Info */
		if(true) {
			System.out.println("INFO : Get state!");
			waitForKeyPress();
			Message msg01 = new Message();
			msg01.put("Opcode", "GetState");
			Message ans01 = performRequest(msg01);
		}
		
		/* Start recording */
		if(true) {
			System.out.println("INFO : Record 2s via mic!");
			Message msg = new Message();
			msg.put("Opcode", "StartRecording");
			msg.put("Duration", "2.0");
			Message ans = performRequest(msg);
		}
		
		/* Play 6000 Hz for 50 ms */
		if(true) {
			System.out.println("INFO : Play signal (1)!");
			Message msg = new Message();
			msg.put("Opcode", "PlayTone");
			msg.put("Frequence", "4000.0");
			msg.put("Duration", "0.05");
			Message ans = performRequest(msg);
		}
		
		/* Wait for play-stop */
		if(true) {
			System.out.println("INFO : Wait for play stop!");
			Message msg = new Message();
			msg.put("Opcode", "WaitForSilence");
			Message ans = performRequest(msg);
		}
		
		/* Wait 100 ms */
		Thread.sleep(100);
		
		/* Play 6000 Hz for 50 ms */
		if(true) {
			System.out.println("INFO : Play signal (2)!");
			Message msg = new Message();
			msg.put("Opcode", "PlayTone");
			msg.put("Frequence", "4000.0");
			msg.put("Duration", "0.05");
			Message ans = performRequest(msg);
		}
		
		/* Wait for play-stop */
		if(true) {
			System.out.println("INFO : Wait for play stop!");
			Message msg = new Message();
			msg.put("Opcode", "WaitForSilence");
			Message ans = performRequest(msg);
		}
		
		/* Wait for record to finish */
		if(true) {
			System.out.println("INFO : Wait for record to finish!");
			Message msg = new Message();
			msg.put("Opcode", "WaitForRecordStopped");
			Message ans = performRequest(msg);
		}
		
		/* Store recorded audio to TMP folder */
		if(true) {
			System.out.println("INFO : Store record to TMP folder!");
			Message msg = new Message();
			msg.put("Opcode", "StoreRecToTmp");
			Message ans = performRequest(msg);
		}
		
		/* Process the signal */
		if(true) {
			System.out.println("INFO : Process audio record!");
			Message msg = new Message();
			msg.put("Opcode", "ProcessAudioRec");
			msg.put("Frequence", "0.0");
			msg.put("Duration", "0.0");
			msg.put("MinThrs", "1000");
			msg.put("PeekNumber", "10");
			msg.put("MaxLocDist", "7000");
			Message ans = performRequest(msg);
			
			for(Entry<String,String> entry : ans.entrySet()) {
				System.out.println( entry.getKey() + " = " + entry.getValue() );
			}
		}
	
		/* TODO : Remove before release */
		if(true) {
			return;
		}
		
		
		
		
		
		
		
		/* REQ 03 - Wait until playing is over */
		Message msg03 = new Message();
		msg03.put("Opcode", "WaitForSilence");
		Message ans03 = performRequest(msg03);
		
		/* REQ 04 - Play 1000 Hz for 1 sec */
		Message msg04 = new Message();
		msg04.put("Opcode", "PlayTone");
		msg04.put("Frequence", "1000.0");
		msg04.put("Duration", "1.0");
		Message ans04 = performRequest(msg04);
		
		return;	
	}
	
	
	
	
	
	
	
	
	private static Message readIncommingRequest() {
		/* Wait for length-tag */
		int answerLength = 0;
		try {
			StringBuffer strBuf01 = new StringBuffer();
			while(true) {
				char nextChar = (char) ( sLocInpStream.read() );
				if(nextChar != ' ') {
					strBuf01.append(nextChar);
				} else {
					answerLength = Integer.parseInt( strBuf01.toString() );
					break;
				}
			}
		} catch (IOException e) {
			return null;
		}
		
		/* Receive json-String*/
		StringBuffer strBuf02 = new StringBuffer();
		try {
			while(answerLength > 0) {
				char[] tmpInBuffer = new char[answerLength]; 
				int receivedBytes = sLocInpStream.read(tmpInBuffer);
				strBuf02.append(tmpInBuffer, 0, receivedBytes);
				answerLength -= receivedBytes;
			}
		} catch (IOException e) {
			return null;
		}
		
		/* Convert to message */
		return Message.parseJsonObjToMessage( strBuf02.toString() );
	}
	
	private static void responseToRequest(Message respMsg) {
		/* Convert message to json-String */
		String msgJson = respMsg.toJsonString();
		
		/* Send the length-tag */
		String msgLengthTag = String.valueOf( msgJson.length() ) + " ";
		try {
			sLocOutStream.write(msgLengthTag);
			sLocOutStream.flush();
		} catch (IOException e) {
			return;
		}
		
		/* Send the message */
		try {
			sLocOutStream.write(msgJson);
			sLocOutStream.flush();
		} catch (IOException e) {
			return ;
		}
	}
	
	private static void processRequests() throws InterruptedException {
		
		System.out.println(">> SERVER MAIN FUNCTION STARTED !");
		
		while(true) {
			
			/* Read incoming message */
			Message incRequest = readIncommingRequest();
			Message resMsg = new Message();
			
			/* Read Opcode */
			String opcode = incRequest.get("Opcode");
			
			System.out.println("Received command with Opcode (" + opcode + ")");
			
			/* Analyze OpCode-Tag */
			if(opcode.equals("WaitForRecordStopped")) {
				Thread.sleep(2000);
			} else if(opcode.equals("ProcessAudioRec")) {
				resMsg.put("Opcode", "Result");
				resMsg.put("PEAK_0_IDX", "3000");
				resMsg.put("PEAK_0_VALUE", "0.5");
				resMsg.put("PEAK_0_HIT", "0.3");
				
				resMsg.put("PEAK_0_IDX", "5000");
				resMsg.put("PEAK_0_VALUE", "0.5");
				resMsg.put("PEAK_0_HIT", "0.3");
			}
			
			/* Send back a response message */
			responseToRequest(resMsg);
		}
		
	}
	
	private static void waitForKeyPress() {
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
