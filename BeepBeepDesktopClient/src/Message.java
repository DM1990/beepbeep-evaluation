

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Message extends HashMap<String,String> {

	public String toJsonString() {
		JSONObject out = new JSONObject();
		
		for (Map.Entry<String, String> entry : this.entrySet())
		{
		    try {
				out.put(entry.getKey(), entry.getValue());
			} catch (JSONException e) {
				return null;
			}
		}
		
		return out.toString();
	}
	
	public static Message parseJsonObjToMessage(String jsonObj) {
		
		Message resMsg = new Message();
		
		/* Parse JSON-String */
		JSONObject obj = null;
		try {
			obj = new JSONObject(jsonObj);
		} catch (JSONException e) {
			return null;
		}
		
		/* Assign ever key-value pair on 1st level to the map */
		JSONArray nameArray = obj.names();
		
		for(int i = 0; i < nameArray.length(); i++) {
			try {
				String key = nameArray.getString(i);
				resMsg.put(key, obj.getString(key));
			} catch (JSONException e) {
				return null;
			}
		}
		
		return resMsg;
	}
}
