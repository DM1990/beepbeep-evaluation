package com.example.beepbeepevaluation.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.os.Environment;
import android.util.Log;

import com.example.beepbeepevaluation.Globals;

public class PcmStorage {

	public static void perform(
						String 	path,
						short[] pcm) {
		/* Cast array to int[] */
		int[] out = new int[pcm.length];
		for(int i = 0; i < out.length; i++) {
			out[i] = pcm[i] * 32768;
		}
		
		/* Invoke the basic implementation */
		perform(path,out);
	}
	
	public static void perform(
						String 	path,
						int[] 	pcm ) {
		
		/* Check permission granted */
		boolean permGranted = false;
		String fsState = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(fsState)) {
	    	permGranted = true;
	    } else {
	    	permGranted = false;
	    	Log.e(Globals.APP_NAME, "You do not have the required permission for storing files within the system!");
	    }
	    
	    /* Transfer the PCM to the required path */
	    File file = new File(path);
	    
	    FileOutputStream fos = null;
		try {
			
			if(!file.exists())
		    	file.createNewFile();
			
			fos = new FileOutputStream(file);
			
			for(int i = 0; i < pcm.length; i++) {
				fos.write(intToByteArray(pcm[i]));
			}

			fos.flush();
			fos.close();
			
		} catch (FileNotFoundException e) {
			Log.e(Globals.APP_NAME, "Could not open FileOutputStream for " + file.getAbsolutePath() + " !");
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not tranfer the data to the storage => " + e.getMessage() + " !");
		}
	}
	
	public static void perform(
			String 	path,
			float[] pcm ) {

		/* Check permission granted */
		boolean permGranted = false;
		String fsState = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(fsState)) {
		permGranted = true;
		} else {
		permGranted = false;
		Log.e(Globals.APP_NAME, "You do not have the required permission for storing files within the system!");
		}
		
		/* Normalize the signal */
		float[] normPCM = new float[pcm.length];
		float MaxValue = Float.MIN_VALUE;
		
		for(int i = 0; i < pcm.length; i++) {
			if( Math.abs(pcm[i]) > MaxValue )
				MaxValue = Math.abs(pcm[i]);
		}
		
		for(int i = 0; i < normPCM.length; i++) {
			normPCM[i] = pcm[i] / MaxValue; 
		}
		
		/* Transfer the PCM to the required path */
		File file = new File(path);
		
		FileOutputStream fos = null;
		try {
		
		if(!file.exists())
			file.createNewFile();
		
		fos = new FileOutputStream(file);
		
		for(int i = 0; i < pcm.length; i++) {
			fos.write(
					intToByteArray(
							Float.floatToIntBits(pcm[i]) ) );
		}
		
		fos.flush();
		fos.close();
		
		} catch (FileNotFoundException e) {
			Log.e(Globals.APP_NAME, "Could not open FileOutputStream for " + file.getAbsolutePath() + " !");
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not tranfer the data to the storage => " + e.getMessage() + " !");
		}
	}
	
	
	
	public static final byte[] intToByteArray(int value) {
	    return new byte[] {
	            (byte)(value >>> 24),
	            (byte)(value >>> 16),
	            (byte)(value >>> 8),
	            (byte)value};
	}
	
}
