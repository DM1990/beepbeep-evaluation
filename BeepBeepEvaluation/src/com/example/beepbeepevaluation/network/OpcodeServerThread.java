package com.example.beepbeepevaluation.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import com.example.beepbeepevaluation.Globals;
import com.example.beepbeepevaluation.algorithms.CrossCorrelation;
import com.example.beepbeepevaluation.algorithms.MeanConvolution;
import com.example.beepbeepevaluation.algorithms.PeakSearch;
import com.example.beepbeepevaluation.algorithms.PeakSearch.Peak;
import com.example.beepbeepevaluation.algorithms.Shortalyze;
import com.example.beepbeepevaluation.speaker.SignalGenerator;
import com.example.beepbeepevaluation.utils.PcmStorage;

import android.util.Log;

public class OpcodeServerThread extends Thread {

	private Socket mSocket = null;
	
	private boolean isAlive = true;
	
	private InputStreamReader mIn = null;
	private OutputStreamWriter mOut = null;
	
	public OpcodeServerThread(Socket soc) {
		this.mSocket = soc;
	}
	
	public boolean isConnectedToRemoteSide() {
		return this.mSocket.isConnected();
	}
	
	public boolean isStillAlive() {
		return this.isAlive;
	}
	
	public void killSever() {
		this.isAlive = false;
	}
	
	public void run() {
		
		/* Initialize required references */
		try {
			this.mIn = new InputStreamReader(this.mSocket.getInputStream(), "UTF-8");
			this.mOut = new OutputStreamWriter(this.mSocket.getOutputStream(), "UTF-8");
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "OpcodeServerThread could not open IO Streams! (" + e.getMessage() + ")");
			this.isAlive = false;
		}
		
		/* Wait for messages as long, as there are no connection problem */
		while(this.isAlive) {
			
			/* Read the length-tag */
			int requestLength = 0;
			try {
				StringBuffer strBuf01 = new StringBuffer();
				while(true) {
					int nextByte = this.mIn.read();
					
					if(nextByte == -1) {
						throw new Exception("Closed connection while reading length-tag!");
					}
					
					char nextChar = (char) ( nextByte );
					if(nextChar != ' ') {
						strBuf01.append(nextChar);
					} else {
						requestLength = Integer.parseInt( strBuf01.toString() );
						break;
					}
				}
			} catch (Exception e) {
				Log.e(Globals.APP_NAME, "Could not receive the lenth-tag from client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
			
			/* Read the json-string */
			StringBuffer strBuf02 = new StringBuffer();
			try {
				while(requestLength > 0) {
					char[] tmpInBuffer = new char[requestLength]; 
					int receivedBytes = this.mIn.read(tmpInBuffer);
					strBuf02.append(tmpInBuffer, 0, receivedBytes);
					requestLength -= receivedBytes;
				}
			} catch (IOException e) {
				Log.e(Globals.APP_NAME, "Could not receive the request from client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
			
			/* Convert to message */
			Message requestMsg = Message.parseJsonObjToMessage( strBuf02.toString() );
			
			/* Call handler function */
			Message responseMsg = handleRequest(requestMsg);
			
			/* Convert result to json-string */
			String responseJsonString = responseMsg.toJsonString();
			
			/* Transmit the length-tag of the result */
			String msgLengthTag = String.valueOf( responseJsonString.length() ) + " ";
			try {
				this.mOut.write(msgLengthTag);
				this.mOut.flush();
			} catch (IOException e) {
				Log.e(Globals.APP_NAME, "Could not send the resonse length-tag to client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
			
			/* Transmit the result */
			try {
				this.mOut.write(responseJsonString);
				this.mOut.flush();
			} catch (IOException e) {
				Log.e(Globals.APP_NAME, "Could not send the response to the client! (" + e.getMessage() + ")");
				this.isAlive = false;
				continue;
			}
		}
		
		/* Close socket */
		try {
			this.mSocket.close();
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not close the socket of the OpcodeServerThread! (" + e.getMessage() + ")");
		}
		
		/* Thread says goodbye */
		this.isAlive = false;
	}
	
	private Message handleRequest(Message in) {
		
		Message out = new Message();
		
		/* Read the opcode of request */
		String reqOpcode = in.get("Opcode");
		if(reqOpcode == null) {
			out.put("error", "The request did not contain an opcode!");
			return out;
		}
		
		/* Handle the opcode */
		
		/* OPCODE : Get state of system */
		if( reqOpcode.equals("GetState") ) {
			hdnlOpcodeGetState(in, out);
			
		/* OPCODE : Get state of system */
		} else if( reqOpcode.equals("PlayTone") ) {
			hdnlOpcodePlayTone(in, out);
			
		/* OPCODE : Wait until playback had been terminated */
		} else if( reqOpcode.equals("WaitForSilence") ) {
			hdnlOpcodeWaitForSilence(in, out);
		
		/* OPCODE : Start recording audio signal */
		} else if( reqOpcode.equals("StartRecording") ) {
			hdnlOpcodeStartRecording(in, out);		
		/* OPCODE : Wait until recording process has been stopped */
		} else if( reqOpcode.equals("WaitForRecordStopped") ) {
			hdnlOpcodeWaitForRecordStopped(in, out);
		/* OPCODE : Process audio record buffer */
		} else if( reqOpcode.equals("ProcessAudioRec") ) {
			hdnlOpcodeProcessAudioRec(in,out);
		/* OPCODE : Store the recorded PCM to temp folder */
		} else if( reqOpcode.equals("StoreRecToTmp") ) {
			hdnlOpcodeStoreRecToTmp(in,out);
		/* OPCODE : Play chirp signal */
		} else if( reqOpcode.equals("MP.PlayChirp") ) {
			hdnlOpcodeProcessPlayChirp(in,out);
		/* OPCODE : Wait for MP-Silence */
		} else if( reqOpcode.equals("MP.WaitForSilence") ) {
			hdnlOpcodeProcessMpWaitForSilence(in,out);
		/* Default behavior if opcode is unknown to the system */
		} else {
			out.put("error", "The passed opcode could not be resolved!");
		}
		
		/* Return the result */
		out.put("Opcode", reqOpcode);
		out.put("Response", "true");
		return out;
	}

	
	
	/* Opcode Handlers */
	
	private void hdnlOpcodeWaitForRecordStopped(Message in, Message out) {
		Globals.SubSystems.MICROPHONE_CONTROLLING.waitForStop();
		
	}

	private void hdnlOpcodeStartRecording(Message in, Message out) {
		float duration = Float.parseFloat( in.get("Duration") );
		Globals.SubSystems.MICROPHONE_CONTROLLING.startRecord(duration);
	}

	private void hdnlOpcodeGetState(Message in, Message out) {
		/* Get state of audio recorder */
		boolean recorderAlive = Globals.SubSystems.MICROPHONE_CONTROLLING.isAlive();
		
		/* Get state of audio speaker */
		boolean speakerAlive = Globals.SubSystems.SPEAKER_CONTROLLING.isAlive();
		
		/* Append both results to the message */
		out.put("MicrophoneState", String.valueOf(recorderAlive));
		out.put("SpeakerState", String.valueOf(speakerAlive));
	}
	
	private void hdnlOpcodePlayTone(Message in, Message out) {
		float frequence = Float.parseFloat( in.get("Frequence") );
		float duration = Float.parseFloat( in.get("Duration") );
		
		Globals.SubSystems.SPEAKER_CONTROLLING.playTone(frequence, duration);
	}
	
	private void hdnlOpcodeWaitForSilence(Message in, Message out) {
		Globals.SubSystems.SPEAKER_CONTROLLING.waitForStopState();
	}
	
	private void hdnlOpcodeProcessAudioRec(Message in, Message out) {
		
		/* Convert values from input message */
		float inMinThrs 		= Float.parseFloat( in.get("MinThrs") );
		float inPeekNumber 		= Float.parseFloat( in.get("PeekNumber") );
		float inMaxLocDist 		= Float.parseFloat( in.get("MaxLocDist") );
		
		/* Process signal */
		/* Load PCM file of record*/
		short[] recPcmSignal = Globals.SubSystems.MICROPHONE_CONTROLLING.getRecordData();
		
		/* Load PCM file of reference signal */
		short[] refPcmSignal = SignalGenerator.perform(
													44100, 
													4000.0f, 
													0.05f);
		
		/* Perform CrossCorrelation */
		float[] cc = CrossCorrelation.perform(recPcmSignal, refPcmSignal);
		
		/* Store the cross correlation results */
		if(false) {
			PcmStorage.perform(
					createNewFileName("CC","pcm"), 
					cc );
		}
		
		/* Calculate the maximum peak value */
		int maxCross = CrossCorrelation.getMaxCrossCorrelation(refPcmSignal);
		System.out.println("Maximum Cross Correlation : " + maxCross);
		
		/* Sortalyze the cc-Signal */
//		short[] ccShort = Shortalyze.perform(cc);
//		if(false) {
//			PcmStorage.perform(
//					createNewFileName("CCS","pcm"), 
//					ccShort );
//		}
		
		/* Find the peak values */
		PeakSearch.InputParams inpPeak = new PeakSearch.InputParams();
		inpPeak.inMinThrs = (int) inMinThrs;
		inpPeak.inPeekNumber = (int) inPeekNumber;
		inpPeak.inMaxLocDist = (int) inMaxLocDist;
		inpPeak.inSignal = cc;
		inpPeak.inMaxPeakValue = maxCross;
		ArrayList<Peak> outR = PeakSearch.perform(inpPeak);
		
		
		/* Print out found peaks */
		System.out.println("\nFound " + outR.size() + " peaks! \n");
		for(Peak p : outR) {
			System.out.println("PEAK_IDX : " + p.SampleNumber + " PEAK_VAL : " + p.PeakValue + " HIT : " + p.PeakRelValue);
		}
		
		/* Send back the result structure */
		out.put("Opcode", "Result");
		
		for(int i = 0; i < outR.size(); i++) {
			String prefix = "PEAK_" + i;
			Peak p = outR.get(i);
			out.put(
				prefix + "_IDX",
				Integer.toString(p.SampleNumber) );
			
			out.put(
				prefix + "_VALUE",
				Float.toString(p.PeakValue) );
			
			out.put(
					prefix + "_HIT",
					Float.toString(p.PeakRelValue) );
		}
	}
	
	private void hdnlOpcodeStoreRecToTmp(Message in, Message out) {
		
		/* Get configured path */
		String expPath = Globals.ACTIVITY_REF.getSharedPreferences( Globals.CONFIG_NAME, 0 ).getString("System.TempFolder", null);
		if(expPath == null) {
			Log.e(Globals.APP_NAME, "No path for temporary folder had been specified! => Export impossible!");
			return;
		} else {
			expPath = expPath + Long.toHexString((new Random()).nextLong()) + ".pcm";
		}
		
		/* Store the content to a predifined path */
		Globals.SubSystems.MICROPHONE_CONTROLLING.storeRecordedPcm(expPath);
	}
	
	private void hdnlOpcodeProcessPlayChirp(Message in, Message out) {
		Globals.SubSystems.MEDIA_PLAYER_CONTROLLING.playSound();
	}
	
	private void hdnlOpcodeProcessMpWaitForSilence(Message in, Message out) {
		Globals.SubSystems.MEDIA_PLAYER_CONTROLLING.waitForSilence();
	}
	
	private String createNewFileName(
								String fileCategory,
								String fileType ) {
		String expPath = Globals.ACTIVITY_REF.getSharedPreferences( Globals.CONFIG_NAME, 0 ).getString("System.TempFolder", null);
		if(expPath == null) {
			Log.e(Globals.APP_NAME, "No path for temporary folder had been specified! => Export impossible!");
			return null;
		} else {
			expPath = expPath + fileCategory + "_" + Long.toHexString((new Random()).nextLong()) + "." + fileType;
		}
		
		return expPath;
	}
}
