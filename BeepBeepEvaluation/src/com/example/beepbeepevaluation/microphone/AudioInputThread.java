package com.example.beepbeepevaluation.microphone;

import java.util.concurrent.Semaphore;


import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

public class AudioInputThread extends Thread {
	
	/* Class configuration */
	private static final int MAX_SAMPLE_PER_ITER = 20000; 
	private static final int SAMPLING_RATE = 44100;
	
	/* PCM Buffer */
	private short[] mPcmBuffer = new short[0];
	private int		mPcmBufferCurPos = 0;
	
	/* Audio subsystem references */
	private AudioRecord mAudioRecord = null;
	
	/* Events */
	private Semaphore mWakeUpSem 	= new Semaphore(0);
	private Semaphore mInitDone 	= new Semaphore(0);
	private Semaphore mRecordStoped	= new Semaphore(0);
	
	/* FSM State */
	private Request mLastReq = Request.INITIALIZE;
	
	/* Error Message */
	private Exception mLastError = null;
	
	public AudioInputThread() {
		this.setName("AudioInputThread");
	}
	
	public void run() {
		
		//Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		
		while(true) {
			switch(mLastReq) {
				case INITIALIZE:
					hndlReqInitialize();
					break;
				case STOP:
					hndlReqStop();
					break;
				case RECORDING:
					hndlReqRecording();
					break;
				case DESTROY:
					hndlReqDestroy();
					return;	
				case ERROR:
					hndlReqError();
					break;
			}
		}
	}
	
	private void hndlReqError() {
		try {
			mWakeUpSem.acquire();
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}	
	}

	private void hndlReqDestroy() {
		return;		
	}

	private void hndlReqRecording() {
		
		/* If record process had just been started */
		if(this.mPcmBufferCurPos == 0) {
			this.mAudioRecord.startRecording();
		}
		
		if(this.mPcmBufferCurPos < this.mPcmBuffer.length) {
			this.mPcmBufferCurPos += mAudioRecord.read(
													mPcmBuffer, 
													mPcmBufferCurPos, 
													this.mPcmBuffer.length - this.mPcmBufferCurPos);
		} else {
			mLastReq = Request.STOP;
		}
	}

	private void hndlReqStop() {
		try {
			mRecordStoped.release();
			mWakeUpSem.acquire();
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}
	}

	private void hndlReqInitialize() {
		this.mAudioRecord = new AudioRecord(
									MediaRecorder.AudioSource.MIC,
									SAMPLING_RATE, 
									AudioFormat.CHANNEL_IN_MONO,
									AudioFormat.ENCODING_PCM_16BIT, 
									AudioRecord.getMinBufferSize(
														SAMPLING_RATE, 
														AudioFormat.CHANNEL_IN_MONO,
														AudioFormat.ENCODING_PCM_16BIT ) 
											);
		
		this.mLastReq = Request.STOP;
		this.mInitDone.release();
	}
	
	/* Public methods */
	public void startRecord(float duration) {
		if(mLastReq == Request.STOP){
			this.mPcmBuffer = new short[ (int)(duration * (float)(SAMPLING_RATE)) ];
			this.mPcmBufferCurPos = 0;
			this.mLastReq = Request.RECORDING;
			this.mWakeUpSem.release();
		}
	}
	
	public void waitForInit() {
		if(this.mLastReq != Request.INITIALIZE)
			return;
		else {
			try {
				this.mInitDone.acquire();
			} catch (InterruptedException e) {
				this.mLastError = e;
				this.mLastReq = Request.ERROR;
			}
		}
	}
	
	public void waitForStop() {
		if(this.mLastReq == Request.STOP) {
			return;
		} else {
			while(true) {
				try {
					this.mRecordStoped.acquire();
				} catch (InterruptedException e) {
					this.mLastError = e;
					this.mLastReq = Request.ERROR;
				}
				if(this.mLastReq == Request.STOP) {
					return;
				}
			}
		}
	}
	
	public short[] getRecordData() {
		waitForStop();
		return this.mPcmBuffer;
	}

	public boolean isStillAlive() {
		if(mLastReq != Request.INITIALIZE && mLastReq != Request.ERROR) {
			return true;
		} else {
			return false;
		}
	}
	
	enum Request {
		INITIALIZE,
		STOP,
		RECORDING,
		DESTROY,
		ERROR
	}
}
