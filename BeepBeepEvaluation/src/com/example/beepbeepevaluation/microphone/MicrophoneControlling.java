package com.example.beepbeepevaluation.microphone;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import com.example.beepbeepevaluation.Globals;

import android.os.Environment;
import android.util.Log;

public class MicrophoneControlling {

	private static MicrophoneControlling SINGLETON = null;
	
	private AudioInputThread mAudioInputThread = null;
	
	/* This class should be singleton */
	private MicrophoneControlling() {
		mAudioInputThread = new AudioInputThread();
		mAudioInputThread.start();
	}
	
	public static MicrophoneControlling getInstance() {
		if(SINGLETON == null)
			SINGLETON = new MicrophoneControlling();
		
		return SINGLETON;
	}
	
	/* Public interface */
	public void startRecord(float duration) {
		this.mAudioInputThread.startRecord(duration);
	}
	
	public void waitForInit() {
		this.mAudioInputThread.waitForInit();
	}
	
	public void waitForStop() {
		this.mAudioInputThread.waitForStop();
	}
	
	public short[] getRecordData() {
		return this.mAudioInputThread.getRecordData();
	}
	
	public boolean isAlive() {
		return this.mAudioInputThread.isStillAlive();
	}
	
	public void storeRecordedPcm(String path) {
		short[] pcm = this.getRecordData();
		
		/* Check permission granted */
		boolean permGranted = false;
		String fsState = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(fsState)) {
	    	permGranted = true;
	    } else {
	    	permGranted = false;
	    	Log.e(Globals.APP_NAME, "You do not have the required permission for storing files within the system!");
	    }
	    
	    /* Convert short-array to byte-array */
	    ByteBuffer byteBuf = ByteBuffer.allocate(pcm.length * 2);
	    for(int i = 0; i < pcm.length; i++)
	    	byteBuf.putShort(pcm[i]);
	    
	    byte[] pcmByte = byteBuf.array();
	    
	    /* Transfer the PCM to the required path */
	    File file = new File(path);
	    
	    FileOutputStream fos = null;
		try {
			
			if(!file.exists())
		    	file.createNewFile();
			
			fos = new FileOutputStream(file);
			fos.write(pcmByte);
			fos.flush();
			fos.close();
			
		} catch (FileNotFoundException e) {
			Log.e(Globals.APP_NAME, "Could not open FileOutputStream for " + file.getAbsolutePath() + " !");
		} catch (IOException e) {
			Log.e(Globals.APP_NAME, "Could not tranfer the data to the storage => " + e.getMessage() + " !");
		}
	}
}
