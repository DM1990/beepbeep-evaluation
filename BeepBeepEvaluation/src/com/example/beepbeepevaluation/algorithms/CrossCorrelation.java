package com.example.beepbeepevaluation.algorithms;

import java.util.ArrayList;

import com.example.beepbeepevaluation.Globals;
import com.example.beepbeepevaluation.utils.PcmStorage;

public class CrossCorrelation {

	private static final boolean DEBUG = true;
	
	public static class InputParam {
		public short[] inRecSignal;
		public short[] inRefSignal;
		public int inTimeOffset;
		public int inMinIncPerStep;
		public int inFirstStopSample;
	}
	
	public static float perform( InputParam inp ) {
		
		float Acc = 0;
		
		short[] recSignal = inp.inRecSignal;
		short[] refSignal = inp.inRefSignal;
		
		/* Calculate range of iteration */
		int idxFirst = inp.inTimeOffset;
		int idxLast = Math.min(	inp.inRecSignal.length, 
								inp.inTimeOffset + inp.inRefSignal.length );
		
		for(int idxRecCur = idxFirst, idxRefCur = 0; idxRecCur < idxLast; idxRefCur++, idxRecCur++) {
			Acc += (float) ( (recSignal[idxRecCur] * refSignal[idxRefCur]) );
			
			/* Nice optimization but requires strong analyze */
			if( false && (idxRefCur % 100 == 0) && (idxRefCur > inp.inFirstStopSample) && (inp.inMinIncPerStep * idxRefCur > Acc) )
				return 0;
		}
		
		return Acc;
	}
	
	public static float[] perform( 	short[] inRecSignal,
									short[] inRefSignal  ) {
		float[] out = new float[inRecSignal.length];
		
		InputParam p = new InputParam();
		p.inRecSignal = inRecSignal;
		p.inRefSignal = inRefSignal;
		p.inFirstStopSample = 250;
		p.inMinIncPerStep = 0;
		
		/* Variables for normalization */
		float NormMaxValue = Float.MIN_VALUE;
		
		
		for(int i = 0; i < out.length; i++) {
			
			if( false && (i % 1000 == 0) )
				System.out.println("Perform the " + i + " Sample!");
			
			/* Optimization */
			boolean stepFast = true;
			
			if( (i + 100) < inRecSignal.length ) {
				if(Math.abs(inRecSignal[i]) < 10) {
					for(int s = 0; s < 100; s++) {
						if(Math.abs(inRecSignal[i+s]) > 10) {
							stepFast = false;
							break;
						}
					}
				} else {
					stepFast = false;
				}
			} else {
				stepFast = false;
			}
			
			if(!stepFast) {
				p.inTimeOffset = i;
				out[i] = perform(p);
				
				if( Math.abs(out[i]) > NormMaxValue )
					NormMaxValue = Math.abs(out[i]);
			} else {
				i += 50;
			}
		}
		
		/* Perform normalization */
		//for(int i = 0; i < out.length; i++) {
			//out[i] /= NormMaxValue;
		//}
		
		if(DEBUG) {
			/* Take path for TMP folder */
			String expPath = Globals.ACTIVITY_REF.getSharedPreferences( Globals.CONFIG_NAME, 0 ).getString("System.TempFolder", null);
			
			
			String pathCRC = expPath + "CRC_SIGNAL.raw";
			String pathREC = expPath + "REC_SIGNAL.raw";
			
			System.out.println("PATH : " + pathCRC);
			System.out.println("PATH : " + pathREC);
			
			/* Write CRC signal */
			PcmStorage.perform( pathCRC, out );
			
			/* Write REC signal */
			PcmStorage.perform( pathREC, inRecSignal );
		}
		
		return out;
	}
	
	public static int getMaxCrossCorrelation( short[] inRefSignal ) {
		
		if(inRefSignal == null) {
			return 0;
		}
		/* Go through the reference signal, calculate the ^2 value
		 * and return the accumulation */
		else {
			int acc = 0;
			
			for(int i = 0; i < inRefSignal.length; i++) {
				acc += inRefSignal[i] + inRefSignal[i];
			}
			
			return acc;
		}
	}
	
}
