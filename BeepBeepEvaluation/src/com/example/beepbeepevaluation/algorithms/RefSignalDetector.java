package com.example.beepbeepevaluation.algorithms;

import java.util.ArrayList;

import com.example.beepbeepevaluation.Globals;
import com.example.beepbeepevaluation.algorithms.PeakSearch.Peak;
import com.example.beepbeepevaluation.speaker.SignalGenerator;
import com.example.beepbeepevaluation.utils.PcmStorage;

public class RefSignalDetector {
	
	public static ArrayList<Peak> performDetection() {
		
		float inMinThrs 		= 0.0f;
		float inPeekNumber 		= 10.0f;
		float inMaxLocDist 		= 7000f;
		
		/* Process signal */
		/* Load PCM file of record*/
		short[] recPcmSignal = Globals.SubSystems.MICROPHONE_CONTROLLING.getRecordData();
		
		/* Load PCM file of reference signal */
		short[] refPcmSignal = SignalGenerator.perform(
													44100, 
													4000.0f, 
													0.05f);
		
		/* Perform CrossCorrelation */
		float[] cc = CrossCorrelation.perform(recPcmSignal, refPcmSignal);
		
		/* Calculate the maximum peak value */
		int maxCross = CrossCorrelation.getMaxCrossCorrelation(refPcmSignal);
		System.out.println("Maximum Cross Correlation : " + maxCross);
		
		/* Sortalyze the cc-Signal */
		//short[] ccShort = Shortalyze.perform(cc);
		
		/* Find the peak values */
		PeakSearch.InputParams inpPeak = new PeakSearch.InputParams();
		inpPeak.inMinThrs = (int) inMinThrs;
		inpPeak.inPeekNumber = (int) inPeekNumber;
		inpPeak.inMaxLocDist = (int) inMaxLocDist;
		inpPeak.inSignal = cc;
		inpPeak.inMaxPeakValue = maxCross;
		ArrayList<Peak> outR = PeakSearch.perform(inpPeak);
		
		return outR;
	}
	
}
