package com.example.beepbeepevaluation.algorithms;

import java.util.Arrays;

public class Shortalyze {
	
	public static short[] perform(int[] input) {
		
		/* Find the max value of input */
		int[] sortedInp = input.clone();
		Arrays.sort(sortedInp);
		
		int minValAbs = sortedInp[0];
		minValAbs = minValAbs * Integer.signum(minValAbs);
		
		int maxValAbs = sortedInp[sortedInp.length - 1];
		maxValAbs = maxValAbs * Integer.signum(maxValAbs);
		
		float absMax = (float) Math.max(minValAbs, maxValAbs);
		
		
		/* Allocate the output array */
		short[] out = new short[input.length];
		
		/* Normalize the values */
		for(int i = 0; i < sortedInp.length; i++) {
			out[i] = (short) (( ((float) input[i]) / absMax) * ((float) Short.MAX_VALUE)); 
		}
		
		return out;
	}
	
}
