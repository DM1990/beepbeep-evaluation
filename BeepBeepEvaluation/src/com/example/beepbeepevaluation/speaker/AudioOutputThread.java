package com.example.beepbeepevaluation.speaker;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

public class AudioOutputThread extends Thread {
	
	/* Class configuration */
	private static final int MAX_SAMPLE_PER_ITER = 44100 * 5; 
	private static final int SAMPLING_RATE = 44100;
	
	/* PCM Buffer */
	private short[] mPcmBuffer = new short[0];
	private int		mPcmBufferCurPos = 0;
	
	/* FSM State */
	private Request mLastReq = Request.INITIALIZE;
	
	/* Audio subsystem references */
	private AudioTrack mAudioTrack = null;
	private int mAudioTrackBufferSize = 0;
	
	/* Events */
	private Semaphore mWakeUpSem 		= new Semaphore(0);
	private Semaphore mInitDone 		= new Semaphore(0);
	private Semaphore mStopStateReached = new Semaphore(0);
	
	/* Error Message */
	private Exception mLastError = null;
	
	public AudioOutputThread() {
		this.setName("AudioOutputThread");
	}
	
	public short[] getPCMPlaybackBuffer() {
		return this.mPcmBuffer;
	}
	
	public void run() {
		
		//Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		
		while(true) {
			switch(mLastReq) {
				case INITIALIZE:
					hndlReqInitialize();
					break;
				case STOP:
					hndlReqStop();
					break;
				case PLAY:
					hndlReqPlay();
					break;
				case PAUSE:
					hndlReqPause();
					break;
				case DESTROY:
					hndlReqDestroy();
					return;	
				case ERROR:
					hndlReqError();
					break;
			}
		}
	}
	
	/* Request handler */
	private void hndlReqInitialize() {
		
		this.mAudioTrackBufferSize = AudioTrack.getMinBufferSize(
										SAMPLING_RATE, 
										AudioFormat.CHANNEL_OUT_MONO,
										AudioFormat.ENCODING_PCM_16BIT ); 

		this.mAudioTrack = new AudioTrack(
								AudioManager.STREAM_MUSIC, 
								SAMPLING_RATE, 
								AudioFormat.CHANNEL_OUT_MONO,
								AudioFormat.ENCODING_PCM_16BIT, 
								this.mAudioTrackBufferSize, 
								AudioTrack.MODE_STREAM); 
		
		if(this.mAudioTrack == null) {
			this.mLastError = new Exception("Could not initialize the AudioTrack!");
			mLastReq = Request.ERROR;
		} else {
			this.mAudioTrack.play();
		}
		
		if(this.mPcmBuffer.length > 0) {
			mLastReq = Request.PLAY;
		} else {
			mLastReq = Request.STOP;
		}
		
		mInitDone.release();
	}
	
	private void hndlReqStop() {
		
		try {
			if(this.mStopStateReached.availablePermits() == 0)
				this.mStopStateReached.release();
			
			mWakeUpSem.acquire();
			
			if(this.mStopStateReached.availablePermits() > 0)
				this.mStopStateReached.acquire();
			
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}
		return;
	}
	
	private void hndlReqPause() {
		try {
			mWakeUpSem.acquire();
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}
		return;
	}
	
	private void hndlReqPlay() {
		
		/* Warm up the cheap speaker */
		if(this.mPcmBufferCurPos == 0) {
			mAudioTrack.write(
					new short[this.mAudioTrackBufferSize], 
					0, 
					this.mAudioTrackBufferSize );
		}
		
		int samplesToWrite = Math.min(this.mPcmBuffer.length - this.mPcmBufferCurPos, MAX_SAMPLE_PER_ITER);
		
		if(samplesToWrite > 0) {
			this.mPcmBufferCurPos += mAudioTrack.write(mPcmBuffer, mPcmBufferCurPos, samplesToWrite);
		} else {
			/* Android sucks as there is the following problem :
			 * If the buffer is not filled completely, it do not start playing.
			 * Begging for this did not helped, so I decided following ...
			 * 1) If PCM-Buffer is not of n*BufferSize length I add zeros as long as the buffer is full
			 * 2) If PCM-Buffer is of the right size, everything if fine
			 * */
			if(this.mPcmBuffer.length % this.mAudioTrackBufferSize != 0) {
				int PcmToInsert = this.mPcmBuffer.length % this.mAudioTrackBufferSize;
				short[] zeroPcm = new short[PcmToInsert];
				mAudioTrack.write(zeroPcm, 0, PcmToInsert);
			}
			mLastReq = Request.STOP;
		}
		return;
	}
	
	private void hndlReqDestroy() {
		mAudioTrack.stop(); 
		mAudioTrack.release();
		return;
	}
	
	private void hndlReqError() {
		try {
			mWakeUpSem.acquire();
		} catch (InterruptedException e) {
			mLastError = e;
			mLastReq = Request.ERROR;
		}
	}
	
	/* Public methods */
	public void playPcm(short[] pcm) {
		if( (pcm != null) && (this.mLastReq != Request.PLAY) ) {
			this.mPcmBuffer = pcm;
			this.mPcmBufferCurPos = 0;
			this.mLastReq = Request.PLAY;
			this.mWakeUpSem.release();
		}
	}
	
	public void stopPlaying() {
		this.mLastReq = Request.STOP;
	}
	
	public void waitForInit() {
		if(this.mLastReq != Request.INITIALIZE) {
			return;
		} else {
			try {
				mInitDone.acquire();
			} catch (InterruptedException e) {
				mLastError = e;
				mLastReq = Request.ERROR;
			}
		}
	}
	
	public void waitForStopState() {
		if(this.mLastReq == Request.STOP)
			return;
		else {
			try 
			{
				this.mStopStateReached.acquire();
			} catch (InterruptedException e) {
				mLastError = e;
				mLastReq = Request.ERROR;
			}
		}
	}
	
	public void playTone(float frequence, float duration) {
		if( (this.mLastReq != Request.PLAY)
				&& (frequence > 0)
				&& (duration > 0) ) 
		{
			
			this.mPcmBuffer = SignalGenerator.perform(SAMPLING_RATE, frequence, duration);
			this.mPcmBufferCurPos = 0;
			
			this.mLastReq = Request.PLAY;
			this.mWakeUpSem.release();
		}
	}
	
	public boolean isStillAlive() {
		if(this.mLastReq != Request.INITIALIZE && this.mLastReq != Request.ERROR) {
			return true;
		} else {
			return false;
		}
	}
	
	enum Request {
		INITIALIZE,
		STOP,
		PAUSE,
		PLAY,
		DESTROY,
		ERROR
	}
}
