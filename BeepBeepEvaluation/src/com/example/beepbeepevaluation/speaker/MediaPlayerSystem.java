package com.example.beepbeepevaluation.speaker;

import java.util.concurrent.Semaphore;

import com.example.beepbeepevaluation.Globals;
import com.example.beepbeepevaluation.R;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

public class MediaPlayerSystem extends Thread {

	private static MediaPlayerSystem mInstance = null;
	
	private MediaPlayerSystem() {
		this.setName("MediaPlayerSystem");
	}
	
	/**
	 * Class members
	 * */
	/* TODO */
	
	/**
	 * Object members
	 * */
	
	/* Synchronisation members */
	private Semaphore mNewCommand = new Semaphore(0);
	private Semaphore mReadyForNextCommand = new Semaphore(1);
	private CommandEnum mLastCommand = CommandEnum.WAITING;
	private Semaphore mPlayCompleted = new Semaphore(1);
	
	/* MediaPlayer instance */
	private MediaPlayer mMediaPlayer = null;
	
	/**
	 * Public Methods
	 * */
	
	public static MediaPlayerSystem getInstance() {
		if(mInstance == null) {
			mInstance = new MediaPlayerSystem();
			mInstance.start();
		}
		
		return mInstance;
	}
	
	/* THREAD FUNCTION */
	public void run() {
		try {
			
			/* Initialize subsystem */
			hndlInitialize();
			
			/* Processing loop */
			while(true) {
				/* Wait for new command */
				this.mNewCommand.acquire();
				
				/* Process the command */
				switch(mLastCommand) {
					case PLAY:
						hndlCmdPlay();
						break;
					case DEINIT:
						hndlCmdDeinit();
						break;
					case WAITING:
						/* Do nothing */
						break;
					default:
						break;
				}
				
				/* Reset the command */
				this.mLastCommand = CommandEnum.WAITING;
				
				/* Ready for next command */
				mReadyForNextCommand.release();
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void playSound() {
		try {
			mPlayCompleted.acquire();
			mReadyForNextCommand.acquire();
			this.mLastCommand = CommandEnum.PLAY;
			this.mNewCommand.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void waitForSilence() {
		try {
			mPlayCompleted.acquire();
			mPlayCompleted.release();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deinit() {
		try {
			mReadyForNextCommand.acquire();
			this.mLastCommand = CommandEnum.DEINIT;
			this.mNewCommand.release();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Private Methods
	 * */
	private void hndlInitialize() {
		/* Create MediaPlayer instance */
		this.mMediaPlayer = MediaPlayer.create(
										Globals.ACTIVITY_REF, 
										R.raw.reference_signal_2k_6k );
		
		/* Register a completion handler */
		final MediaPlayerSystem mpS = this;
		this.mMediaPlayer.setOnCompletionListener(new OnCompletionListener() 
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
            	mpS.mPlayCompleted.release();
            }
        });
	}
	
	private void hndlCmdPlay() {
		this.mMediaPlayer.start();
	}
	
	private void hndlCmdDeinit() {
		
	}
}

enum CommandEnum {
	INITIALIZE,
	PLAY,
	DEINIT,
	WAITING
}
