package com.example.beepbeepevaluation.speaker;

public class SpeakerControlling {

	private static SpeakerControlling SINGLETON = null;
	
	private AudioOutputThread mAudioOutputThread = null;
	
	/* This class should be singleton */
	private SpeakerControlling() {
		mAudioOutputThread = new AudioOutputThread();
		mAudioOutputThread.start();
	}
	
	public static SpeakerControlling getInstance() {
		if(SINGLETON == null)
			SINGLETON = new SpeakerControlling();
		
		return SINGLETON;
	}
	
	/* Public interface */
	public void playPcm(short[] pcm) {
		mAudioOutputThread.playPcm(pcm);
	}
	
	public void stopPlaying() {
		mAudioOutputThread.stopPlaying();
	}
	
	public void waitForInit() {
		mAudioOutputThread.waitForInit();
	}
	
	public void playTone(float frequence, float duration) {
		mAudioOutputThread.playTone(frequence, duration);
	}
	
	public boolean isAlive() {
		return this.mAudioOutputThread.isStillAlive();
	}
	
	public void waitForStopState() {
		this.mAudioOutputThread.waitForStopState();
	}
	
	public short[] getPCMPlaybackBuffer() {
		return this.mAudioOutputThread.getPCMPlaybackBuffer();
	}
}
