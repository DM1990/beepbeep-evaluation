package com.example.beepbeepevaluation;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.widget.EditText;

public class ConfigurationInputDialog extends AlertDialog.Builder {

	private String 				mConfigKey 		= null;
	private String 				mDiagTitel 		= "UNDEFINED";
	private String 				mDiagMsg 		= "UNDEFINED";
	private EditText 			mInput			= null;	
	private Context				mContext		= null;
	private SharedPreferences 	mPreferences 	= null;
	
	public ConfigurationInputDialog(Context ctx) {
		super(ctx);
		this.mContext = ctx;
	}
	
	public void setConfigKey(String key) {
		this.mConfigKey = key;
	}
	
	public void setDialogTitel(String titel) {
		if(titel != null)
			this.mDiagTitel = titel;
	}
	
	public void setDialogMessage(String msg) {
		if(msg != null)
			this.mDiagMsg = msg;
	}
	
	public AlertDialog show() {
		/* Prepare dialog */
		this.setTitle(mDiagTitel);
		this.setMessage(mDiagMsg);
		this.mInput = new EditText(mContext);
		this.setView(mInput);
		this.setPositiveButton("OK", new PositiveButtonHandler() );
		this.setNegativeButton("ABORT", new NegativeButtonHandler() );
		this.mPreferences = this.mContext.getSharedPreferences( Globals.CONFIG_NAME, 0 );
		
		/* Check if there is already a preference entry */
		String curKeyVal = mPreferences.getString(this.mConfigKey, null);
		if(curKeyVal != null)
			this.mInput.setText(curKeyVal);
		
		/* Continue with default behavior */
		return super.show();
	}
	
	/* Handler for accepting new configuration */
	class PositiveButtonHandler implements DialogInterface.OnClickListener {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			ConfigurationInputDialog outRef = ConfigurationInputDialog.this;
			SharedPreferences.Editor edit = outRef.mPreferences.edit();
			edit.putString( 
					outRef.mConfigKey , 
					outRef.mInput.getText().toString() );
			edit.commit();
			
		}
		
	}
	
	/* Handler for refuse new configuration */
	class NegativeButtonHandler implements DialogInterface.OnClickListener {

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			
		}
		
	}

}
