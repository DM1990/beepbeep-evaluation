// Load Reference Signal
[Ref_Y,Ref_Fs,Ref_Bits] = wavread("C:/Temp/ReferenceSignal_2k_6k.wav");

// Load Recorded Signal
[Rec_Y,Rec_Fs,Rec_Bits] = wavread("C:/Users/Daniil/Documents/Meine empfangenen Dateien/GALAXY/REC_SIGNAL.wav");

// Calculate cross correlation
[CrossCorrelation] = xcorr(Rec_Y,Ref_Y,"none");

// Plot cross correlation
//plot(Rec_Y)
plot(1:size(CrossCorrelation,2),CrossCorrelation)
