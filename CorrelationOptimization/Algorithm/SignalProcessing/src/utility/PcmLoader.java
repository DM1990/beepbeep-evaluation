package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class PcmLoader {

	public static short[] loadPCM(String path) {
		
		/* Check if a path had been passed */
		if(path == null)
			return null;
		
		/* Check if path leads to an already existing file */
		File pcmFile = new File(path);
		
		if( (!pcmFile.isFile()) || (!pcmFile.exists()) )
			return null;
		
		/* Read the content of the whole file and convert to short values */
		byte[] byteInput = null;
		short[] pcmInput = null;
		int pcmCurPos = 0;
		try {
			/* Detect amount of samples */
			long fileSize = pcmFile.length();
			int sampleAmount = (int) (fileSize / Short.BYTES);
			
			FileInputStream in = new FileInputStream(pcmFile);
			byteInput = new byte[(int) fileSize];
			pcmInput = new short[sampleAmount];
			
			/* Read the file content */
			if( in.read(byteInput) != fileSize ) {
				System.out.println("ERROR : File could not be read completely");
				return null;
			}
			
			ByteBuffer shortConverter = ByteBuffer.allocate(Short.BYTES);
			for(int i = 0; i < fileSize; i += 2) {
				shortConverter.put(byteInput[i+1]);
				shortConverter.put(byteInput[i]);
				pcmInput[i/2] = shortConverter.getShort(0);
				shortConverter.rewind();
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		/* Return the short-Array */
		return pcmInput;
	}

	public static void storePCM(
							short[] pcm, 
							String 	path ) {
		int[] out = new int[pcm.length];
		for(int i = 0; i < out.length; i++) {
			out[i] = pcm[i] * 65535;
		}
		
		storePCM(out,path);
	}
	
	public static void storePCM(
							int[] 	pcm, 
							String 	path ) {
		
		try {
			
			File f = new File(path);
			
			FileOutputStream fout = new FileOutputStream(f);
			
			for(int i = 0; i < pcm.length; i++) {
				fout.write( ByteBuffer.allocate(Integer.BYTES).putInt(pcm[i]).array() ); 
			}
			
			fout.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
