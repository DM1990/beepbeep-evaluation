package utility;

public class SignalGenerator {

	/* Warm up sequence */
	private static float WARMUP_DURATION_SEC = 0.005f;
	private static float WARMUP_FREQUENCE_HZ = 2000f;
	
	public static short[] perform(
							int		inSamplingRate,
							float 	inFrequence,
							float 	inDuration) {
		
		/* Calculate the size of the output buffer */
		int sampleNumber = (int) (((float) inSamplingRate) * inDuration);
		float SecPerSample = (float) (1.0 / ((float)inSamplingRate));
		
		/* Calculate Warm-Up sequence length */
		int sampleWarmUpLength = (int) (((float) inSamplingRate) * WARMUP_DURATION_SEC);
		
		/* Create output buffer */
		short[] outPcmBuffer = new short[sampleWarmUpLength + sampleNumber];
		
		/* Generate Warm-Up sequence */
		for(int i = 0; i < sampleWarmUpLength; i++) {
			float relTime = SecPerSample * ((float)i);
			outPcmBuffer[i] = (short) (((float)Short.MAX_VALUE) * Math.sin( relTime * WARMUP_FREQUENCE_HZ * 2.0 *  Math.PI));
		}
		
		/* Calculate the PCM values */
		for(int i = 0; i < sampleNumber; i++) {
			float relTime = SecPerSample * ((float)i);
			float freqPercent = 2000.0f + inFrequence * (float)( (float)i / (float)sampleNumber);
			outPcmBuffer[i + sampleWarmUpLength] = (short) (((float)Short.MAX_VALUE) * Math.sin( relTime * freqPercent * 2.0 *  Math.PI));
		}
		
		return outPcmBuffer;
	}
	
}
