package algorithm;

public class MeanConvolution {

	public static class InputParams {
		public short[] 	inSignal;
		public int 		inWindowsSize;
	}
	
	public static short[] perform( InputParams inp ) {
		
		/* Allocate the memory for the output signal */
		short[] inSignal = inp.inSignal;
		short[] outSignal = new short[inp.inSignal.length];
		
		int twoWindowSize = inp.inWindowsSize * 2;
		
		int wL = inp.inWindowsSize * (-2);
		int wMid = inp.inWindowsSize * (-1);
		int wR = 0;
		
		int curVal = 0;
		
		while(wMid < 0) {
			curVal += Math.abs(inSignal[wR]);
			wL++;
			wMid++;
			wR++;
		}
		
		while(wL < 0) {
			curVal += Math.abs(inSignal[wR]);
			outSignal[wMid] = (short) (curVal / twoWindowSize);
			wL++;
			wMid++;
			wR++;
		}
		
		while(wR < inSignal.length) {
			curVal += Math.abs(inSignal[wR]);
			curVal -= Math.abs(inSignal[wL]);
			outSignal[wMid] = (short) (curVal / twoWindowSize);
			wL++;
			wMid++;
			wR++;
		}
		
		while(wMid < inSignal.length) {
			curVal -= Math.abs(inSignal[wL]);
			outSignal[wMid] = (short) (curVal / twoWindowSize);
			wL++;
			wMid++;
			wR++;
		}
		
		/* Return result */
		return outSignal;
	}
}
