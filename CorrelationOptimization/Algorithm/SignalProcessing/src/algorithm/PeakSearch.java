package algorithm;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;

public class PeakSearch {

	public static class InputParams {
		public short[] 	inSignal;
		public int 		inMinThrs;
		public int		inPeekNumber;
		public int		inMaxLocDist;
		public int		inMaxPeakValue;
	}
	
	public static class Peak implements Comparable<Peak> {
		public int 		SampleNumber;
		public int 		PeakValue;
		public float 	PeakRelValue;
		
		@Override
		public int compareTo(Peak arg0) {
			return arg0.PeakValue - this.PeakValue;
		}
	}
	
	public static ArrayList<Peak> perform( InputParams inp ) {
		
		ArrayList<Peak> peakList = new ArrayList<Peak>();
		
		short[] inSignal = inp.inSignal;
		int inSignalLength = inp.inSignal.length;
		int minThrs = inp.inMinThrs;
		int maxDistToLocMax = inp.inMaxLocDist;
		
		int curIdx = 0;
		boolean maxTriggerActive = false;
		int locMaxIdx = 0;
		int locMaxVal = 0;
		int locMaxDist = 0;
		
		while(curIdx < inSignalLength) {
			int curVal = inSignal[curIdx];
			if(!maxTriggerActive) {
				if(curVal > minThrs) {
					maxTriggerActive = true;
					locMaxIdx = curIdx;
					locMaxVal = curVal;
					locMaxDist = 0;
				}
			} else {
				if(curVal > locMaxVal) {
					locMaxIdx = curIdx;
					locMaxVal = curVal;
					locMaxDist = 0;
				} else if(locMaxDist < maxDistToLocMax) {
					locMaxDist++;
					
					if(locMaxDist == maxDistToLocMax) {
						maxTriggerActive = false;
						Peak np = new Peak();
						np.PeakValue = locMaxVal;
						np.SampleNumber = locMaxIdx;
						np.PeakRelValue = ((float) np.PeakValue) / ((float) inp.inMaxPeakValue);
						peakList.add(np);
						locMaxIdx = 0;
						locMaxVal = 0;
						locMaxDist = 0;
					}
				}
			}
			curIdx++;
		}
		
		/* Attach last max value if it had not been added within the while loop */
		if(locMaxVal != 0) {
			Peak np = new Peak();
			np.PeakValue = locMaxVal;
			np.SampleNumber = locMaxIdx;
			np.PeakRelValue = ((float) np.PeakValue) / ((float) inp.inMaxPeakValue);
		}
		
		/* Sort the peak array by peak-value */
		Collections.sort(peakList);
		
		/* Filter the first n peaks (n is specified in the input parameter) */
		ArrayList<Peak> outPeaks = new ArrayList<Peak>();
		while( (outPeaks.size() <= inp.inPeekNumber)
				&& (outPeaks.size() < peakList.size() ) )
			outPeaks.add( peakList.get(outPeaks.size()) );
		
		return outPeaks;
	}

	
	public static void plotPeaks(	short[]			inRecSignal,
									ArrayList<Peak> inPeaks, 
									String 			inPath ) {
		
		short[] out = new short[inRecSignal.length];
		
		for(Peak p : inPeaks) {
			out[p.SampleNumber] = Short.MAX_VALUE;
		}
		
		try {
			
			File f = new File(inPath);
			
			FileOutputStream fout = new FileOutputStream(f);
			
			for(int i = 0; i < out.length; i++) {
				fout.write( ByteBuffer.allocate(Short.BYTES).putShort(out[i]).array() ); 
			}
			
			fout.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
