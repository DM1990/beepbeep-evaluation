package exceptions;

public class SigProcException extends Exception {

	public SigProcException(String msg) {
		super(msg);
	}
	
}
