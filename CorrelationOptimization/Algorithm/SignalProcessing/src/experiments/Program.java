package experiments;

import java.util.ArrayList;
import java.util.Arrays;

import algorithm.MeanConvolution;
import algorithm.PeakSearch;
import algorithm.PeakSearch.Peak;
import utility.PcmLoader;

public class Program {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/* Load PCM file */
		short[] recPcmSignal = PcmLoader.loadPCM("C:\\Users\\Daniil\\Documents\\DM\\02 - Projekte\\005 - Bitbucket (IT-Project)\\CorrelationOptimization\\Recorded Signals\\Test002_Static.pcm");
		
		/* Print max and min values */
		short[] sortedPCMSignal = recPcmSignal.clone();
		Arrays.sort(sortedPCMSignal);
		
		System.out.println("Signal Min Value : " + sortedPCMSignal[0] );
		System.out.println("Signal Max Value : " + sortedPCMSignal[sortedPCMSignal.length - 1] );
		
		/* Perform convolution */
		MeanConvolution.InputParams inp = new MeanConvolution.InputParams();
		inp.inSignal = recPcmSignal;
		inp.inWindowsSize = 40;
		short[] convResult = MeanConvolution.perform(inp);
				
		/* Perform peak search */
		PeakSearch.InputParams inpPeak = new PeakSearch.InputParams();
		inpPeak.inMinThrs = 1000;
		inpPeak.inPeekNumber = 11;
		inpPeak.inMaxLocDist = 7000;
		inpPeak.inSignal = recPcmSignal;
		ArrayList<Peak> out = PeakSearch.perform(inpPeak);
		
		/* Print out found peaks */
		System.out.println("\nFound " + out.size() + " peaks! \n");
		for(Peak p : out) {
			System.out.println("PEAK_IDX : " + p.SampleNumber + " PEAK_VAL : " + p.PeakValue);
		}
	
	}

}
