package experiments;

import java.util.ArrayList;
import java.util.Arrays;

import utility.PcmLoader;
import utility.SignalGenerator;
import algorithm.CrossCorrelation;
import algorithm.MeanConvolution;
import algorithm.PeakSearch;
import algorithm.Shortalyze;
import algorithm.PeakSearch.Peak;

public class Test001 {
	
	public static void main(String[] args) {
		
		/* Load PCM file of record*/
		short[] recPcmSignal = PcmLoader.loadPCM("C:\\Temp\\Test_003.raw");
		
		/* Load PCM file of reference signal */
		short[] refPcmSignal = SignalGenerator.perform(
													44100, 
													4000.0f, 
													0.05f );
		
		PcmLoader.storePCM(refPcmSignal, "C:\\Temp\\testsig.pcm");
		
		/* Perform CrossCorrelation */
		int[] cc = CrossCorrelation.perform(recPcmSignal, refPcmSignal);
		
		/* Calculate the maximum peak value */
		int maxCross = CrossCorrelation.getMaxCrossCorrelation(refPcmSignal);
		System.out.println("Maximum Cross Correlation : " + maxCross);
		
		/* Find the peak values */
		PeakSearch.InputParams inpPeak = new PeakSearch.InputParams();
		inpPeak.inMinThrs = 1000;
		inpPeak.inPeekNumber = 10;
		inpPeak.inMaxLocDist = 7000;
		inpPeak.inSignal = Shortalyze.perform(cc);
		inpPeak.inMaxPeakValue = maxCross;
		ArrayList<Peak> out = PeakSearch.perform(inpPeak);
		
		
		/* Print out found peaks */
		System.out.println("\nFound " + out.size() + " peaks! \n");
		for(Peak p : out) {
			System.out.println("PEAK_IDX : " + p.SampleNumber + " PEAK_VAL : " + p.PeakValue + " HIT : " + p.PeakRelValue);
		}
		
		/* Export cross correlation */
		PcmLoader.storePCM(
					cc, 
					"C:\\Users\\Daniil\\Documents\\DM\\02 - Projekte\\005 - Bitbucket (IT-Project)\\CorrelationOptimization\\Feedback Signal\\Feedback001.pcm" );
		
		/* Create feedback signal */
		if(true) {
			PeakSearch.plotPeaks(
							recPcmSignal, 
							out, 
							"C:\\Users\\Daniil\\Documents\\DM\\02 - Projekte\\005 - Bitbucket (IT-Project)\\CorrelationOptimization\\Feedback Signal\\Feedback002.pcm" );
		}
	}
	
}
